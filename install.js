var spawn = require('child_process').spawn;
var path = require('path');
var extend = require('util')._extend;
var config = require('./configure');
var log = require('winston');
var async = require('async');

module.exports = function(options, cb) {
    log.info('Running Install for dotnet-core-web app: %s', options.name);

    options.port = options.port || options.install.port || 80;
    options.siteName = options.siteName || options.install.siteName || "${environment}-${name}",
    options.hostHeader =  options.hostHeader || options.install.hostHeader || '*';
    options.vdir = options.vdir || options.install.vdir || null;

    //If env settings supplied, write the settings and then return the installer.
    if (options.env) {
        config.set(options, function(er, r) {

            return async.waterfall([
                run_iis_install(options),
                open_firewall_port(options)
            ], cb);

        });
    }
	
    //if env settings NOT supplied, read the current settings and run than run the installer.
    else {
        config.get(options, function(er, r) {

            return async.waterfall([
                run_iis_install(options),
		            open_firewall_port(options)
            ], cb);

        })
    }
}

function run_iis_install(options) {
    return (cb) => {
        runInstall(options, cb);
    }
}


function open_firewall_port(options) {
    return (cb) => {

      var port = options.port;
      var args = `advfirewall firewall add rule name="Website Port ${port}" dir=in action=allow protocol=TCP localport=${port}`;

      log.info('netsh', args);

      var cmd = spawn('netsh', args.split(' '));
      var hasError;
      var dataOutput = '';

      cmd.stdout.on('data', (data) => {
          dataOutput += data;
          log.verbose(data.toString());
      });

      cmd.stderr.on('data', (data) => {
          hasError = true;
          log.error(data.toString());
      });

      cmd.on('close', (code) => {

          if (hasError || code > 0) {
              log.info(dataOutput);
              return cb('An error occured during the web publish. Exit Code: ' + code);
          }

          return cb();
      });
    }
}



function runInstall(options, cb) {

    /*
    iis_install
    * Website Name
    * Port
    * Host Headers
    * vdir
    * enable32bit
    * b - additional bindings
    * dir  - current direc

    */

    options.install = options.install || {};

    var expander = options.fn.variableExpander.resolver(extend({
        name: options.name || options.project,
        PRAZZLE_ENV: options.environment || 'Default',
        environment: options.environment || 'Default'
    }, options.env));

    var hostHeader = getHostHeader(expander.resolve((options.hostHeader)));

    var cmdOptions = {
        name: expander.resolve(options.siteName),
        port: options.port,
        host: hostHeader,
        vdir: options.vdir,
        dir: path.resolve(path.join(process.cwd(), options.install.dir || '')),
    };

    log.verbose("IIS install:", cmdOptions);

    var args = [];
    for (var k in cmdOptions) {
        if (cmdOptions[k]) args.push('--' + k + '=' + cmdOptions[k]);
    }
    console.log(cmdOptions);
    if (options.install.enable32Bit) args.push('--enable32Bit');

    log.verbose('Running IISWebsiteInstall with args: %s', args.join(' '))
    var cmd = spawn(path.resolve(path.join(__dirname, 'iiswebsiteinstaller/IISWebsiteInstall.exe')), args);
    var hasError;
    var dataOutput = '';

    cmd.stdout.on('data', (data) => {
        dataOutput += data;
        log.verbose(data.toString());
    });

    cmd.stderr.on('data', (data) => {
        hasError = true;
        log.error(data.toString());
    });

    cmd.on('close', (code) => {

        if (hasError || code > 0) {
            log.info(dataOutput);
            return cb('An error occured during the web publish. Exit Code: ' + code);
        }

        return cb();
    });
}

var URL = require('url').parse;

function getHostHeader(text) {
    if (text.indexOf("://") === 4 || text.indexOf("://") === 5) {
        var url = new URL(text);
        return url.host;
    }
    return text;
}