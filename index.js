var path = require('path');
var fs = require('fs');
var log = require('winston');
var async = require('async');
var extend = require('util')._extend;


/*
Copy

*/
exports.package = function(options, cb){
	require('./package.js')(prepareOptions(options), cb);
}


exports.publish = function(options, cb){
	require('./publish.js')(prepareOptions(options), cb);
}

exports.configure = {};
exports.configure.get = function(options, cb){
	require('./configure.js').get(prepareOptions(options), cb);
}
exports.configure.set = function(options, cb){
	require('./configure.js').set(prepareOptions(options), cb);
}


exports.publish = function(options, cb){

	require('./publish.js')(prepareOptions(options), cb);
}


exports.install = function(options, cb){
	console.log('Wilson');
	require('./install.js')(prepareOptions(options), cb);
}

exports.setupLocalIis = function(options, cb) {
	require('./setupLocalIis.js')(extend({}, options), cb);
}

function packageFilepath(options){
	return path.join(options.outputDir, packageFilename(options, options) );
}

function packageFilename(options){
	return [options.project, '-', options.version, '.zip'].join('');
}


function packageEnvFilepath(options){
	return path.join(options.outputDir,   [options.project, '-', options.version, '.env'].join('') );
}


function readPackageJson(){
	var projectJsonPath = path.resolve('project.json');
	if(!fs.existsSync(projectJsonPath)) throw "Can't find project.json. Looked here: " + projectJsonPath;
	return require(projectJsonPath);
}


function gitVersion(options, cb){
	options.fn.gitVersion(options.packageJson.version, function(error, v){

		if(error) return  cb(error);

		options.version = v;

		options.packageFilepath = packageFilepath(options);
        options.packageFilename = packageFilename(options);
        options.packageEnvFilepath = packageEnvFilepath(options);

		cb(null, options);
	});
}


function prepareOptions(options){
	options.log = log;

	options.packageJson = readPackageJson();
	options.outputDir = options.outputDir || ['..','dist', options.project].join(path.sep);

	options.packageJson.deploy = options.packageJson.deploy || { };
	gitVersion(options, function(err, options) {
		
        options.packageFilepath = packageFilepath(options);
        options.packageFilename = packageFilename(options);
        options.packageEnvFilepath = packageEnvFilepath(options);

    });

	return options;
}
